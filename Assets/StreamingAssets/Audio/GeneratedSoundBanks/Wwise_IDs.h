/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID ENGINE = 268529915U;
        static const AkUniqueID TIRES = 3386657602U;
    } // namespace EVENTS

    namespace SWITCHES
    {
        namespace SWITCH_GEAR
        {
            static const AkUniqueID GROUP = 3747976225U;

            namespace SWITCH
            {
                static const AkUniqueID GEAR_OFF = 3153319926U;
                static const AkUniqueID GEAR_ON = 3181757496U;
            } // namespace SWITCH
        } // namespace SWITCH_GEAR

        namespace SWITCH_INT_ACCEL
        {
            static const AkUniqueID GROUP = 928546034U;

            namespace SWITCH
            {
                static const AkUniqueID OFF = 930712164U;
                static const AkUniqueID ON = 1651971902U;
            } // namespace SWITCH
        } // namespace SWITCH_INT_ACCEL

    } // namespace SWITCHES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID RTPC_GEAR = 1692101114U;
        static const AkUniqueID RTPC_INT_CONE = 3180956924U;
        static const AkUniqueID RTPC_INT_DECEL_SC = 2344389281U;
        static const AkUniqueID RTPC_INT_GEARSHIFT = 2705590080U;
        static const AkUniqueID RTPC_LOAD = 1193280819U;
        static const AkUniqueID RTPC_POWER = 2978654722U;
        static const AkUniqueID RTPC_RPM = 3774151474U;
        static const AkUniqueID RTPC_SPEED = 1381474336U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MASTER = 4056684167U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
